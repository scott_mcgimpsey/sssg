from io import BytesIO
import win32clipboard
from PIL import Image
import pyvisa as visa
from infi.systray import SysTrayIcon
from subprocess import Popen
from pathlib import Path
from datetime import datetime
import tkinter as tk
from tkinter import messagebox
from tkinter import simpledialog
import sys
import os


def errormsg(title,message):
    root = tk.Tk()
    root.overrideredirect(1)
    root.withdraw()
    messagebox.showerror(title, message)
    root.destroy()

def infomsg(title,message):
    root = tk.Tk()
    root.overrideredirect(1)
    root.withdraw()
    messagebox.showinfo(title, message)
    root.destroy()

def prompt(title,message):
    root = tk.Tk()
    root.overrideredirect(1)
    root.withdraw()
    value = simpledialog.askstring(title,message)
    root.destroy()
    return value

def lecroy_connect(rm,ip):
    connectionstring = "TCPIP0::{}::INSTR".format(ip)
    lecroy = None
    try:
        lecroy = rm.open_resource(connectionstring)
    except Exception as E:
        errormsg('Lecroy Connection Error', 'Unable to connect to the Lecroy scope. Check IP configuration settings, and that LXI (VXI11) remote access is enabled, then try again.\n\nFull Error Message:\n{}'.format(E))    
    return lecroy

# Function to convert image data to RBG
def png_to_rgb(image_data):
    image = Image.open(BytesIO(image_data))
    output = BytesIO()
    image.convert("RGB").save(output, "BMP")
    data = output.getvalue()[14:]
    output.close()
    return data

# Function to push data to clipboard
def send_to_clipboard(clip_type, data):
    win32clipboard.OpenClipboard()
    win32clipboard.EmptyClipboard()
    win32clipboard.SetClipboardData(clip_type, data)
    win32clipboard.CloseClipboard()

# Function to grab screenshot data from oscilloscope
def grab_scope_screenshot(scope_resource):
    # Send Oscilloscope Commands to Configure Screen Image Format
    scope_resource.write("HCSU DEV, JPEG, AREA, DSOWINDOW, PORT, NET")
    scope_resource.write("SCDP")
    # Read the Binary Data
    screen_data = scope_resource.read_raw()
    # if screen_data == b'WARNING : CURRENT REMOTE CONTROL INTERFACE IS TCPIP\n':
    #     raise Exception(screen_data)
    return screen_data

# Main menu operation to put screenshot on clipboard
def main_operation_copy_screenshot_to_clipboard(lecroy):
    # Open Session to Scope with LXI Connection with VISA resource string
    image = grab_scope_screenshot(lecroy)
    data = png_to_rgb(image)
    send_to_clipboard(win32clipboard.CF_DIB, data)

# Main menu operation to save screenshot to file
def main_operation_save_file_to_location(lecroy,filepath,prefix):
    # Open Session to Scope with LXI Connection with VISA resource string
    # Make directory if it doesn't exist
    filepath.mkdir(parents=True, exist_ok=True)
    filenumberindexer = 0
    date = datetime.now().strftime("%Y_%m_%d-%I-%M_%S_%p")
    filename = "{}_{}_{}.png".format(prefix,date,filenumberindexer)
    filewritepath = filepath / filename
    while filewritepath.is_file():
        filenumberindexer += 1
        filename = "{}_{}_{}.png".format(prefix,date,filenumberindexer)
        filewritepath = filepath / filename

    image = grab_scope_screenshot(lecroy)
    f = open(filewritepath, 'wb+')
    f.write(image)
    f.close()


# Main menu operation to open image directory
def main_operation_open_image_directory(filepath):
    filepath.mkdir(parents=True, exist_ok=True)
    Popen('explorer "{}"'.format(filepath))

def main_operation_set_scope_ip(ip):
    new_ip = prompt('Scope IP','Scope IP is currently set as {}.\nUpdate IP Address for Scope:'.format(ip))
    if new_ip is not None and new_ip != '':
        return new_ip
    else:
        return ip


# Configuration / Resource manager prep
rm = visa.ResourceManager()
file_save_path = Path.home() / "Pictures" / "ScopeGrab"
file_prefix = 'oscilloscope'
ip = '192.168.10.100'


# Menu functions
def menu_about(systray):
    infomsg("About Scott's Scope Screen Grabber","Version 1.0 - 10/25/2021\nReport bugs and feature requests to scott.mcgimpsey@aei.com.\nTested using a Lecroy Waverunner 104Mxi. YMMV.")

def menu_oscilloscope_to_clipboard(systray):
    global ip
    lecroy = lecroy_connect(rm,ip)
    if lecroy is not None:
        main_operation_copy_screenshot_to_clipboard(lecroy)
        lecroy.close()

def menu_oscilloscope_to_file(systray):
    global file_save_path
    global file_prefix
    lecroy = lecroy_connect(rm,ip)
    if lecroy is not None:
        main_operation_save_file_to_location(lecroy,file_save_path,file_prefix)
        lecroy.close()

def menu_open_image_directory(systray):
    main_operation_open_image_directory(file_save_path)

def menu_set_scope_ip(systray):
    global ip
    ip = main_operation_set_scope_ip(ip)

def menu_whereami(systray):
    infomsg("whereami?",Path.cwd())

menu_options = (
    ("Copy Scope Sceen to Clipboard",None,menu_oscilloscope_to_clipboard),
    ("Save Scope Screen to File",None,menu_oscilloscope_to_file),
    ("Open Savefile Directory",None,menu_open_image_directory),
    ("Configure Scope IP",None,menu_set_scope_ip),
    ("Scott's Scope Screen Grabber v1.0",None,menu_about),
    #debug function ("Where am I running from?",None,menu_whereami),
)

if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
    bundle_dir = getattr(sys, '_MEIPASS', os.path.abspath(os.path.dirname(__file__)))
    iconpath = os.path.abspath(os.path.join(bundle_dir,'sssg.ico'))
else:
    iconpath = Path.cwd() / "sssg.ico"
iconpath = str(iconpath) # infi.systray doesn't have handling for pathlib paths in some error handling -- Require reconversion to string.

systray = SysTrayIcon(iconpath, "SSSG", menu_options)
systray.start()