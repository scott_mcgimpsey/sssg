# README #

### What is this repository for? ###

* SSSG is a compilable app which runs in the Windows system tray.
  It provides a simple path to grab screenshots from compatible Lecroy Oscilloscopes.
  Screenshots can be stored in the filesystem, or directly on the clipboard to be pasted into documents.

* Version 1.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Compilation Steps ###

* Execute the full_project_rebuild.bat script

### Who do I talk to? ###

* For feature requests or bug reports, contact Scott McGimpsey
* scott.mcgimpsey@aei.com